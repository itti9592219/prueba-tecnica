import 'package:flutter/material.dart';
import 'package:prueba_tecnica_khevin_roig/theme/app_theme.dart';
import 'package:prueba_tecnica_khevin_roig/ui_helpers.dart';

/// Mostar dialogo para notificar al usuario de un evento importante. No es dismissable.
Future<void> showNotificationDialog({
  Function? confirmAction,
  required String title,
  required String subTitle,
}) async {
  await showDialog(
    context: navigatorKey.currentContext!,
    builder: (context) {
      return AlertDialog(
        content: SizedBox(
          width: double.maxFinite,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: [
              // Titulo
              Text(
                title,
                style: AppTheme.getStyle(TitleFont.medium),
              ),

              // Contenido (subtitulo)
              Flexible(
                child: SingleChildScrollView(
                  child: Container(
                    alignment: Alignment.center,
                    margin: const EdgeInsets.all(8.0),
                    child: Text(
                      subTitle,
                      textAlign: TextAlign.center,
                      style: AppTheme.getStyle(
                        BodyFont.large,
                        color: Colors.blueGrey.shade500,
                      ),
                    ),
                  ),
                ),
              ),

              Divider(
                color: Colors.grey.shade500,
                thickness: 0.7,
              ),

              // Botones
              Container(
                padding: const EdgeInsets.only(right: 8, left: 8, top: 16),
                alignment: Alignment.center,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    // Boton Aceptar
                    InkWell(
                      onTap: () {
                        if (confirmAction != null) {
                          confirmAction();
                        } else {
                          navigatorKey.currentState!.pop();
                        }
                      },
                      child: Container(
                        width: 128,
                        height: 40,
                        padding: const EdgeInsets.symmetric(horizontal: 8),
                        decoration: BoxDecoration(
                          border: Border.all(color: AppTheme.primary),
                          borderRadius:
                          const BorderRadius.all(Radius.circular(4)),
                          color: AppTheme.primary,
                        ),
                        alignment: Alignment.center,
                        child: Text(
                          'Aceptar',
                          style: AppTheme.getStyle(
                            ButtonFont.medium,
                            color: Colors.white,
                          ),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      );
    },
    barrierDismissible: false,
    useRootNavigator: false,
    barrierLabel: '',
  );
}