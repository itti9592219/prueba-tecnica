import 'package:flutter/material.dart';

class ActiveClientScreen extends StatelessWidget {
  final String clientName;

  const ActiveClientScreen({Key? key, required this.clientName})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(clientName),
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: const [
          Icon(Icons.person, color: Colors.green,size: 100.0,),
           Center(
            child: Text(
              "Este cliente está activo." ,
              style: TextStyle(
                fontSize: 30.0,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ],
      ),
    );
  }
}