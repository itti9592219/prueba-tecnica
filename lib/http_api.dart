import 'dart:convert';

import 'package:http/http.dart' as http;

const Map<String, String> _header = {
  'Content-type': 'application/json',
  'Accept': 'application/json',
};

Future<List<dynamic>> fetchData() async {
  const url = 'https://reqres.in/api/users';

  final response = await http.get(headers: _header, Uri.parse(url));

  if (response.statusCode == 200) {
      final responseBody = utf8.decode(response.bodyBytes);
      final jsonMap = json.decode(responseBody) as Map<String, dynamic>;
      final jsonData = jsonMap['data'] as List<dynamic>;
      return jsonData;
  } else {
    throw Exception('Failed to fetch data');
  }
}