import 'dart:math';

import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:prueba_tecnica_khevin_roig/client_status/active_client_screen.dart';
import 'package:url_launcher/url_launcher.dart';

import '../client_status/blocked_client_screen.dart';
import '../client_status/inactive_client_screen.dart';


class HomeScreen extends StatefulWidget {

 const HomeScreen({super.key, required this.clientList});
 
 final List<dynamic> clientList;

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  
  

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Lista de Clientes'),
      ),
      body: widget.clientList.isEmpty
          ? const Center(
        child: CircularProgressIndicator(),
      ): Center(
        child: Table(
              border: TableBorder.all(),
              children: [
                const TableRow(children: [
                  Center(child: Text(style: TextStyle(fontSize: 25.0, fontWeight: FontWeight.bold),'Avatar')),
                  Center(child: Text(style: TextStyle(fontSize: 25.0, fontWeight: FontWeight.bold),'Cliente')),
                ],
                ),
                for(final client in widget.clientList)
                  TableRow(
                    children: [
                      TableCell(
                          child: GestureDetector(
                            onTap: (){
                              int number = Random().nextInt(101);
                              String clientName = client['first_name']+" "+client['last_name'];
                              if(number<20){
                                Navigator.push(context, MaterialPageRoute(builder: (context) => ActiveClientScreen(clientName: clientName)));
                              }else if(number%20 == 0){
                                Navigator.push(context, MaterialPageRoute(builder: (context) => InactiveClientScreen(clientName: clientName)));
                              }else{
                                Navigator.push(context, MaterialPageRoute(builder: (context) => BlockedClientScreen(clientName: clientName)));
                              }
                            },
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Container(width: 50.0, height: 50.0, child: Image.network(client['avatar'])),
                            ),
                          ),
                        ),
                      TableCell(
                        child: GestureDetector(
                          onTap: (){
                            int number = Random().nextInt(101);
                            String clientName = client['first_name']+" "+client['last_name'];
                            if(number<20){
                              Navigator.push(context, MaterialPageRoute(builder: (context) => ActiveClientScreen(clientName: clientName)));
                            }else if(number%20 == 0){
                              Navigator.push(context, MaterialPageRoute(builder: (context) => InactiveClientScreen(clientName: clientName)));
                            }else{
                              Navigator.push(context, MaterialPageRoute(builder: (context) => BlockedClientScreen(clientName: clientName)));
                            }
                          },
                          child: Column(
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(client['first_name']+" "+client['last_name']),),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: GestureDetector(
                                  onTap: () async {
                                    String email = client['email'];
                                    launchUrl(Uri.parse(
                                        "mailto:$email"));
                                  },
                                  child: Text(
                                    client['email'],
                                    style: const TextStyle(
                                      decoration: TextDecoration.underline,
                                      color: Colors.blue,
                                    ),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
              ],
        ),
      ),
    );
  }
}

