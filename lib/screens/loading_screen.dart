import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class LoadingScreen extends StatelessWidget {
  const LoadingScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Scaffold(
      backgroundColor: Colors.blue,
      body: Center(
        child: SpinKitRing(color: Colors.white),
      ),
    );
  }
}

class LoadingOverlay extends StatelessWidget {
  const LoadingOverlay({
    Key? key,
    required this.child,
    this.opacity = 1,
    this.fadeDuration,
    required this.isEnabled,
    this.disableUserPop = true,
  }) : super(key: key);

  final Widget child;
  final double opacity;
  final Duration? fadeDuration;
  final bool disableUserPop;

  /// Controla la visibilidad del loading screen
  final bool isEnabled;

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        // Deshabilitar el control de flujo al usuario
        if (disableUserPop) {
          return !isEnabled;
        }
        return true;
      },
      child: Stack(
        children: [
          child,
          IgnorePointer(
            ignoring: !isEnabled,
            child: AnimatedOpacity(
              opacity: isEnabled ? opacity : 0,
              duration: fadeDuration ?? const Duration(milliseconds: 500),
              child: Stack(
                children: const [
                  ModalBarrier(
                    dismissible: false,
                    color: Colors.blue,
                  ),
                  Center(
                    child: SpinKitRing(color: Colors.white)
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
