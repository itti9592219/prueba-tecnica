import 'package:flutter/material.dart';
import 'package:prueba_tecnica_khevin_roig/http_api.dart';
import 'package:prueba_tecnica_khevin_roig/screens/loading_screen.dart';
import 'package:prueba_tecnica_khevin_roig/screens/home_screen.dart';
import 'package:validators/validators.dart' as validator;
import 'package:prueba_tecnica_khevin_roig/ui_helpers.dart';

import '../aux_widgets.dart';

class LoginScreen extends StatefulWidget {
  const LoginScreen({super.key});

  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final _formKey = GlobalKey<FormState>();
  String? _username;
  String? _password;
  String? errorMessage;

  bool _isLoading = false;

  set isLoading(bool value) {
    setState(() {
      _isLoading = value;
    });
  }

  @override
  Widget build(BuildContext context) {
    return LoadingOverlay(
      isEnabled: _isLoading,
      child: Scaffold(
        appBar: AppBar(
          title: const Center(child: Text('Prueba Técnica Flutter')),
        ),
        body: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Form(
            key: _formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.stretch,
              children: <Widget>[
                TextFormField(
                  decoration: const InputDecoration(labelText: 'Username'),
                  validator: (rawValue) {
                    String value = rawValue!;
                   if(value.isEmpty){
                     return 'Por favor ingrese su nombre de usuario';
                   }
                   else if(!validator.isAlpha(value)){
                     showMessage(context, 'El nombre de usuario no debe contener numeros');
                     return 'Username inválido';
                   }
                  },
                  onChanged: (value){
                    if(RegExp(r'\d').hasMatch(value)){
                      errorMessage = 'Username inválido';
                    }else{
                      errorMessage = null;
                      _username = value;
                    }

                  },
                ),
                const SizedBox(height: 16.0),
                TextFormField(
                  decoration: const InputDecoration(labelText: 'Password'),
                  keyboardType: TextInputType.number,
                  obscureText: true,
                  maxLength: 5,
                  validator: (value) {
                    if (value!.isEmpty) {
                      return 'Por favor ingrese su contraseña';
                    }
                    return null;
                  },
                  onChanged: (value) {
                    _password = value;
                  },
                ),
                const SizedBox(height: 16.0),
                ElevatedButton(
                  onPressed: () {
                    if (_formKey.currentState!.validate()) {
                      _attemptLogin(context);
                    }
                  },
                  child: const Text('Login'),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  void _attemptLogin(context) async {
    if (_isLoading) {
      return;
    }

    // Validar
    if (_username == null) {
      showMessage(context, 'Username vacío o inválido.');
      return;
    } else if (_password == null) {
      showMessage(context, 'Contraseña vacía o inválida.');
      return;
    }

    // Empezar proceso login
    try {
      isLoading = true;

      List<dynamic> clientList = await fetchData();


      // Llevar a home si no hubo errores;.
     Navigator.push(context, MaterialPageRoute(builder: (context) => HomeScreen(clientList: clientList,)));
    } catch (error) {
      showNotificationDialog(
          title: 'Error',
          subTitle:
          error.toString());
    }
    isLoading = false;
  }
}
