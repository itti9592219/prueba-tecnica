import 'package:flutter/material.dart';

import 'app_theme.dart';

TextStyle getDisplayFont(DisplayFont font, double? size, Color? color) {
  switch (font) {
    case DisplayFont.large:
      return TextStyle(
        fontWeight: FontWeight.w700,
        fontSize: size ?? 57,
        fontFamily: 'Inter',
        color: color ?? Colors.black,
      );
    case DisplayFont.medium:
      return TextStyle(
        fontWeight: FontWeight.w500,
        fontSize: size ?? 45,
        fontFamily: 'Inter',
        color: color ?? Colors.black,
      );
    case DisplayFont.small:
      return TextStyle(
        fontWeight: FontWeight.w500,
        fontSize: size ?? 36,
        fontFamily: 'Inter',
        color: color ?? Colors.black,
      );
  }
}

TextStyle getHeadlineFont(HeadlineFont font, double? size, Color? color) {
  switch (font) {
    case HeadlineFont.large:
      return TextStyle(
        fontWeight: FontWeight.w500,
        fontSize: size ?? 32,
        fontFamily: 'Inter',
        color: color ?? Colors.black,
      );
    case HeadlineFont.medium:
      return TextStyle(
        fontWeight: FontWeight.w500,
        fontSize: size ?? 28,
        fontFamily: 'Inter',
        color: color ?? Colors.black,
      );
    case HeadlineFont.small:
      return TextStyle(
        fontWeight: FontWeight.w500,
        fontSize: size ?? 24,
        fontFamily: 'Inter',
        color: color ?? Colors.black,
      );
  }
}

TextStyle getTitleFont(TitleFont font, double? size, Color? color) {
  switch (font) {
    case TitleFont.large:
      return TextStyle(
        fontWeight: FontWeight.w700,
        fontSize: size ?? 22,
        fontFamily: 'Inter',
        color: color ?? Colors.black,
      );
    case TitleFont.medium:
      return TextStyle(
        fontWeight: FontWeight.w700,
        fontSize: size ?? 16,
        fontFamily: 'Inter',
        height: 1.25,
        color: color ?? Colors.black,
      );
    case TitleFont.small:
      return TextStyle(
        fontWeight: FontWeight.w600,
        fontSize: size ?? 14,
        fontFamily: 'Inter',
        color: color ?? Colors.black,
      );
  }
}

TextStyle getLabelFont(LabelFont font, double? size, Color? color) {
  switch (font) {
    case LabelFont.large:
      return TextStyle(
        fontWeight: FontWeight.w500,
        fontSize: size ?? 14,
        fontFamily: 'Roboto',
        color: color ?? Colors.black,
      );
    case LabelFont.medium:
      return TextStyle(
        fontWeight: FontWeight.w500,
        fontSize: size ?? 12,
        fontFamily: 'Roboto',
        color: color ?? Colors.black,
      );
    case LabelFont.small:
      return TextStyle(
        fontWeight: FontWeight.w500,
        fontSize: size ?? 11,
        fontFamily: 'Roboto',
        color: color ?? Colors.black,
      );
  }
}

TextStyle getBodyFont(BodyFont font, double? size, Color? color) {
  switch (font) {
    case BodyFont.large:
      return TextStyle(
        fontWeight: FontWeight.w400,
        fontSize: size ?? 16,
        fontFamily: 'Roboto',
        color: color ?? Colors.black,
      );
    case BodyFont.medium:
      return TextStyle(
          fontWeight: FontWeight.w400,
          fontSize: size ?? 14,
          fontFamily: 'Roboto',
          color: color ?? Colors.black,
          letterSpacing: 0.25);
    case BodyFont.small:
      return TextStyle(
          fontWeight: FontWeight.w400,
          fontSize: size ?? 12,
          fontFamily: 'Roboto',
          color: color ?? Colors.black,
          letterSpacing: 0.25);
  }
}

TextStyle getButtonFont(ButtonFont font, double? size, Color? color) {
  switch (font) {
    case ButtonFont.large:
      return TextStyle(
        fontWeight: FontWeight.w600,
        fontSize: size ?? 20,
        fontFamily: 'Roboto',
        color: color ?? Colors.black,
      );
    case ButtonFont.medium:
      return TextStyle(
        fontWeight: FontWeight.w700,
        fontSize: size ?? 16,
        fontFamily: 'Inter',
        color: color ?? Colors.black,
      );
  }
}
