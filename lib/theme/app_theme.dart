import 'package:flutter/material.dart';

import 'app_fonts.dart';


class AppTheme {
  static const Color primary = Color(0xFFF08624);
  static const Color hyperlink = Color(0xFF2288E3);
  static const Color transparent = Colors.transparent;
  static const Color background = Color(0xFFE5E5E5);

  /// Retorna el styling del font, de acuerdo a lo especificado en el FIGMA del proyecto.
  static TextStyle? getStyle(var font, {double? size, Color? color}) {
    TextStyle? value;
    if (font is DisplayFont) {
      value = getDisplayFont(font, size, color);
    } else if (font is HeadlineFont) {
      value = getHeadlineFont(font, size, color);
    } else if (font is TitleFont) {
      value = getTitleFont(font, size, color);
    } else if (font is LabelFont) {
      value = getLabelFont(font, size, color);
    } else if (font is BodyFont) {
      value = getBodyFont(font, size, color);
    } else if (font is ButtonFont) {
      value = getButtonFont(font, size, color);
    }
    return value;
  }
}

enum DisplayFont {
  large,
  medium,
  small,
}

enum BodyFont {
  large,
  medium,
  small,
}

enum LabelFont {
  large,
  medium,
  small,
}

enum ButtonFont {
  large,
  medium,
}

enum TitleFont {
  large,
  medium,
  small,
}

enum HeadlineFont {
  large,
  medium,
  small,
}
