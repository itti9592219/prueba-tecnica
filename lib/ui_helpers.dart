import 'package:flutter/material.dart';

final GlobalKey<ScaffoldMessengerState> snackbarKey =
GlobalKey<ScaffoldMessengerState>();

/* Manejar screens sin contexto.
navigatorKey.currentState.pushNamed('/someRoute');
*/
final GlobalKey<NavigatorState> navigatorKey = GlobalKey<NavigatorState>();

// Muestra un mensaje al usuario, en la forma de un SnackBar.
void showMessage(context, String message) {
  ScaffoldMessenger.of(context).showSnackBar(SnackBar(content:Text(
      message,
      style: const TextStyle(backgroundColor: Colors.transparent, fontSize: 15.0, color: Colors.white)
  ),));
  }